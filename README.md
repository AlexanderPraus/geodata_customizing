# Geodata Customizing

This repository is for customizing the Masterportal installation. 

### File structure

- The `./geoportal` folder contains the Masterportal code and files for building the Docker image.
- The `./portal-config` folder contains the config files for the Masterportal.
- The `./portal-backend` is a submodule and references the Portal Backend, which filters and serves the config files to the Masterportal based on user authorization.
- LEGACY `./mapserver` is not used anymore, since switching from QGIS to GeoServer.

### CI/CD

The pipeline builds a Masterportal Docker image and deploys it to the Gitlab container registry. It is tagged as `geoportal:staging-${CI_COMMIT_BRANCH}`. When deploying multiple Masterportal instances, each instance should be on a separate branch. Because of this, there will be multiple Masterportal images in the container registry and each instance can use its corresponding image.

## Components

### Masterportal

Masterportal 2.40.1 is the frontend component of the GeoStack. Depending on the use case, different layers and maps can be configured (WMS, WFS, SensorThingsAPI, GeoJSON etc.). Masterportal is configured with three main files:

- services-internet.json
- rest-services-internet.json
- config.json

For comprehensive documentation refer to [masterportal.org](https://www.masterportal.org/dokumentation.html).

Since version 2.37, Masterportal includes a login function to authenticate the user against an OIDC-interface (e.g. Keycloak). After a successful login, a token is saved in the cookies and sent with every service request.

### Portal Backend

The backend was developed in-house and filters the Masterportal config files based on the role of an (not-)authenticated user. The Masterportal frontend loads the config files from this backend by redirecting its requests (specified in the nginx configuration). The backend checks the roles of the user by connecting to the IDM before filtering and serving the files.

## Configuration

### Masterportal Login function

For the login functionality to work, you have to set your IDM URLs in the config.js file as follows:
```json
login: {
        oidcAuthorizationEndpoint: "https://idm.{{ DOMAIN }}.de/auth/realms/{{ REALM }}/protocol/openid-connect/auth",
        oidcTokenEndpoint: "https://idm.{{ DOMAIN }}.de/auth/realms/{{ REALM }}/protocol/openid-connect/token",
        oidcClientId: "public_masterportal",
        oidcScope: "profile email openid",
        oidcRedirectUri: "https://geoportal.{{ DOMAIN }}.de/portal",
        interceptorUrlRegex: "https?://geoportal.{{ DOMAIN }}.de/*" // add authorization to all URLs that match the given regex
}
```

Please replace {{ DOMAIN }} and {{ REALM }} with the corresponding values for your platform installation.

### Masterportal UI

UI elements of the Masterportal are configured in config.json. In this file menu elements can be added or removed, center point, extent and coordinate reference system can be set.

For example, to include login UI and a logo, add the following configuration to the menu:

```json
"menu": {
			...
			"login": {
				"name": "translate#additional:modules.tools.login",
				"icon": "bi-door-open"
			},
			"portalTitle": {
				"logo": "resources/img/htag_logo_bunt.png"
			}	
            ...
}
```

Example for including a layer:

```json
"Themenconfig": {
		"Hintergrundkarten": {
			"Layer": [
				{
					"id": "502",
					"name": "OpenStreetMaps",
					"layerAttribution": "<span>(c) OpenStreetMap contributors</span>",
					"visibility": true
				}
			]
		}
}
```

### Masterportal layer

The available layers are specified in services-internet.json and rest-services-internet.json. They are embedded into the menu in config.json.

### Roles

Available roles must be configured in the IDM. The ds_open_data role is the standard role for open datasets, which are available to every (even not logged in) users. This is an example configuration of an open data layer in services-internet.json:
```json
[
   {
      "id": "502",
      "name": "OSM",
      "url": "https://ows.terrestris.de/osm/service?",
      "typ": "WMS",
      "layers": "OSM-WMS",
      "format": "image/png",
      "version": "1.1.1",
      "singleTile": false,
      "transparent": true,
      "transparency": 0,
      "tilesize": "512",
      "gfiAttributes": "ignore",
      "gfiTheme": "default",
      "legend": "ignore",
      "featureCount": "1",
      "epsg": "EPSG:25833",
      "roles": [
         "ds_open_data"
      ]
   }
]
```
**Important**: each layer has to have a role assigned to it, otherwise it will never be displayed. If the layer should be available to the public, use the ds_open_data role.