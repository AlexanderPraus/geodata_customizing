#!/bin/bash


# Write Environment variables, when DEBUG is set to 1
[[ $DEBUG == "1" ]] && env

# Where to find config.js?
REPO_PATH='/usr/share/nginx/html'
CONFIG_FILE_JS="${REPO_PATH}/config.js"

if [ ! -f "${CONFIG_FILE_JS}" ]; then
  echo "Could not find file config.js"
  exit 1
fi


exec "$@"