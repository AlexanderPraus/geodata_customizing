#!/bin/sh

# Replace the URL of the FUTR-HUB mapserver for environment 'staging' within
# the file services-internet.json.

# Where to find services-internet.json?
REPO_PATH=$(git rev-parse --show-toplevel --sq)
SUBFOLDER='static/resources'
SERVICES_INTERNET="${REPO_PATH}/${SUBFOLDER}/services-internet.json"

# services-internet.json: Set default/old URL.
OLD_MAPSERVER='geoportal.staging.futr-hub.de/mapserver'
# This will be replaced with the following URL.
NEW_MAPSERVER='geoportal.dev.futr-hub.de/mapserver'

if [ ! -f "${SERVICES_INTERNET}" ]; then
  echo "Could not find file services-internet.json"
  exit 1
fi


# Perform string replacement.
sed -i "" "s|${OLD_MAPSERVER}|${NEW_MAPSERVER}|g" "${SERVICES_INTERNET}"

# (Re-)Add project.qgs file
git add "${SERVICES_INTERNET}"


# Paranoia check
grep -e "${OLD_MAPSERVER}" "${SERVICES_INTERNET}"
PARANOIA=$?
if [ $PARANOIA -ne 0 ]; then
  exit 0
else
  echo "ERROR: some replacements were unsuccessful."
  exit 1
fi
