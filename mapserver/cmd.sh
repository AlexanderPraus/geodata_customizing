#!/bin/bash

[[ $DEBUG == "1" ]] && env

find /data/qgis -maxdepth 1 -mindepth 1 -type d | while read dir; do
  PROJECT_QGS="${dir}/project.qgs"

  if [ ! -f "${PROJECT_QGS}" ]; then
    echo "Could not find file project.qgs"
    exit 1
  fi

  # Perform string replacement.
  sed -i "s|FUTR-HUB-MAPSERVER-URL|${ENV_FUTR_HUB_MAPSERVER_URL}|g" "${PROJECT_QGS}"

  # Paranoia check
  grep -e "FUTR-HUB-MAPSERVER-URL" "${PROJECT_QGS}"
  PARANOIA=$?
  if [ $PARANOIA = 0 ]; then
    echo "ERROR: some replacements were unsuccessful."
    echo "${PROJECT_QGS}"
    exit 1
  fi
done


exec /usr/bin/xvfb-run --auto-servernum --server-num=1 /usr/bin/spawn-fcgi -p 5555 -n -d /home/qgis -- /usr/lib/cgi-bin/qgis_mapserv.fcgi
