#!/bin/sh

# Perform the followinbg string replacements in file project.qgs:
#
# - s/dbname=.* host=.* port=[0-9]*/service='qwc_geodb'/g
# - s/authcfg=[0-9a-zAZ]*//g

# Where to find project.qgs?
REPO_PATH=$(git rev-parse --show-toplevel --sq)

# Name of service to be used by PostgreSQL
PG_SERVICE_CONF='qwc_geodb'

find ${REPO_PATH}/data -maxdepth 1 -mindepth 1 -type d | while read dir; do
  PROJECT_QGS="${dir}/project.qgs"

  if [ ! -f "${PROJECT_QGS}" ]; then
    echo "Could not find file project.qgs"
    exit 1
  fi

  # Perform string replacement.
  sed -i "" "s/user='[^\']*'\s\{0,1\}//g" "${PROJECT_QGS}"
  sed -i "" "s/password='[^\']*'\s\{0,1\}//g" "${PROJECT_QGS}"
  sed -i "" "s/dbname=.* host=.* port=[0-9]*/service='${PG_SERVICE_CONF}'/g" "${PROJECT_QGS}"
  sed -i "" "s/authcfg=[0-9a-zAZ]*//g" "${PROJECT_QGS}"
  
  # (Re-)Add project.qgs file
  git add "${PROJECT_QGS}"


  # Paranoia check
  grep \
    -e "user" \
    -e "password" \
    -e "dbname=" \
    -e "host=" \
    -e "port=" \
    -e "authcfg=" \
  PARANOIA=$?
  if [ $PARANOIA = 0 ]; then
    echo "ERROR: some replacements were unsuccessful."
    exit 1
  fi
done